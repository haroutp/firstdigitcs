﻿using System;

namespace FirstDigit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        char firstDigit(string inputString) {
            return inputString.First(char.IsDigit);
        }

    }
}
